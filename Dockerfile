FROM node:7.7.2
MAINTAINER cos <info@cos.ovh>
RUN apt-get update
RUN apt-get install -y git curl
RUN curl -s https://raw.githubusercontent.com/Intervox/node-webp/latest/bin/install_webp | bash
RUN apt-get autoremove && apt-get clean
RUN git clone https://gitlab.com/cospoland/scalegate /scalegate
WORKDIR /scalegate
RUN npm install
EXPOSE 3001
CMD ["node", "/scalegate/index.js"]