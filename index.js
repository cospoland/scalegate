const express = require('express')
const app = express()
const request = require('request')
const cwebp = require('cwebp').CWebp
app.use (function(req, res, next) {
  let data = ''
  req.setEncoding('utf8')
  req.on('data', function(chunk) {
    data += chunk
  })
  req.on('end', function() {
    req.body = data
    next()
  })
})
app.post('/', (req, res) => {
  let requestS = {
    method: 'GET',
    url: req.body,
    encoding: null,
  }
  request(requestS, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      if (body.toString("utf8", 0, 6) == "GIF87a" || body.toString("utf8", 0, 6) == "GIF89a") {
        res.send(body)
        return
      }
      let encoder = new cwebp(body)
      encoder.toBuffer((err, buffer) => {
        if (!err||err === "null"|| err === null | err === undefined||err==='') {
          res.send(buffer)
        } else {
          res.status(500).end()
        }
      })
    } else {
        res.status(500).end()
    }
  })
})
app.listen(3001);
